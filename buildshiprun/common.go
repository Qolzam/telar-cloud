package function

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"github.com/openfaas/openfaas-cloud/sdk"
)

type deployment struct {
	Service                string
	Image                  string
	Network                string
	Labels                 map[string]string  `json:"labels"`
	Limits                 *FunctionResources `json:"limits,omitempty"`
	Requests               *FunctionResources `json:"requests,omitempty"`
	EnvVars                map[string]string  `json:"envVars"` // EnvVars provides overrides for functions.
	Secrets                []string           `json:"secrets"`
	ReadOnlyRootFilesystem bool               `json:"readOnlyRootFilesystem"`
	RegistryAuth           string             `json:"registryAuth"`
	Annotations            map[string]string  `json:"annotations"`
}

type EdgeBasicAuth struct {
	User     string `json:"user_name"`
	Password string `json:"password"`
}

func encrypt(key []byte, message string) (encmess string, err error) {
	plainText := []byte(message)

	block, err := aes.NewCipher(key)
	if err != nil {
		return
	}

	//IV needs to be unique, but doesn't have to be secure.
	//It's common to put it at the beginning of the ciphertext.
	cipherText := make([]byte, aes.BlockSize+len(plainText))
	iv := cipherText[:aes.BlockSize]
	if _, err = io.ReadFull(rand.Reader, iv); err != nil {
		return
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(cipherText[aes.BlockSize:], plainText)

	//returns to base64 encoded string
	encmess = base64.URLEncoding.EncodeToString(cipherText)
	return
}

func decrypt(key []byte, securemess string) (decodedmess string, err error) {
	cipherText, err := base64.URLEncoding.DecodeString(securemess)
	if err != nil {
		return
	}
	fmt.Printf("[INFO] cipherText: %s", cipherText)
	block, err := aes.NewCipher(key)
	if err != nil {
		return
	}

	if len(cipherText) < aes.BlockSize {
		err = errors.New("Ciphertext block size is too short!")
		return
	}

	//IV needs to be unique, but doesn't have to be secure.
	//It's common to put it at the beginning of the ciphertext.
	iv := cipherText[:aes.BlockSize]
	cipherText = cipherText[aes.BlockSize:]

	stream := cipher.NewCFBDecrypter(block, iv)
	// XORKeyStream can work in-place if the two arguments are the same.
	stream.XORKeyStream(cipherText, cipherText)
	fmt.Printf("[INFO] cipherText after: %s", cipherText)

	decodedmess = string(cipherText)
	return
}

// AddBasicAuth to a request by reading secrets when available
func AddBasicAuth(req *http.Request, credentials *EdgeBasicAuth) error {

	req.SetBasicAuth(credentials.User, credentials.Password)
	return nil
}

func ReadBasicAuth(basicAuth *EdgeBasicAuth, cipherKey []byte) (*EdgeBasicAuth, error) {
	var credentials *EdgeBasicAuth

	if len(cipherKey) == 0 {
		return nil, fmt.Errorf("invalid key specified for reading basic auth.")
	}
	fmt.Printf("\n[INFO] basicAuth: %v - basicAuth.User: |%s|", basicAuth, basicAuth.User)
	user, userErr := decrypt(cipherKey, basicAuth.User)
	if userErr != nil {
		return nil, fmt.Errorf("unable to decode user %s", userErr)
	}

	password, passErr := decrypt(cipherKey, basicAuth.Password)
	if passErr != nil {
		return nil, fmt.Errorf("Unable to decode password %s", passErr)
	}

	credentials = &EdgeBasicAuth{
		User:     strings.TrimSpace(string(user)),
		Password: strings.TrimSpace(string(password)),
	}
	fmt.Printf("\n[INFO] credentials: %v", *credentials)

	return credentials, nil
}

func edgeFunctionExists(deploy deployment, gatewayURL string, basicAuth *EdgeBasicAuth) (bool, error) {

	r, _ := http.NewRequest(http.MethodGet, gatewayURL+"system/functions", nil)

	addAuthErr := AddBasicAuth(r, basicAuth)
	if addAuthErr != nil {
		log.Printf("Basic auth error %s", addAuthErr)
	}
	res, err := http.DefaultClient.Do(r)

	if err != nil {
		fmt.Println(err)
		return false, err
	}

	defer res.Body.Close()

	fmt.Println("functionExists status: " + res.Status)
	result, _ := ioutil.ReadAll(res.Body)

	functions := []function{}
	json.Unmarshal(result, &functions)

	for _, function1 := range functions {
		if function1.Name == deploy.Service {
			return true, nil
		}
	}

	return false, err
}

func deployEdgeFunction(deploy deployment, gatewayURL, user, password string) (string, error) {
	cipherKey, readCipherKeyErr := sdk.ReadSecret("cipher-key")
	if readCipherKeyErr != nil {
		return "", fmt.Errorf("couldn't get cipher-key: %s", readCipherKeyErr.Error())
	}
	decBasicAuth, readBasicAuthError := ReadBasicAuth(&EdgeBasicAuth{User: user, Password: password}, []byte(cipherKey))
	if readBasicAuthError != nil {
		return "", fmt.Errorf("couldn't read basic auth: %s", readBasicAuthError.Error())
	}
	fmt.Printf("\n[INFO] deployEdgeFunction- gatewayURL: %s, user: %s , password: %s ", gatewayURL, decBasicAuth.User, decBasicAuth.Password)
	exists, err := edgeFunctionExists(deploy, gatewayURL, decBasicAuth)

	bytesOut, _ := json.Marshal(deploy)

	reader := bytes.NewBuffer(bytesOut)

	fmt.Println("Deploying: " + deploy.Image + " as " + deploy.Service)
	var res *http.Response
	var httpReq *http.Request
	var method string
	if exists {
		method = http.MethodPut
	} else {
		method = http.MethodPost
	}

	httpReq, err = http.NewRequest(method, gatewayURL+"system/functions", reader)
	httpReq.Header.Set("Content-Type", "application/json")

	addAuthErr := AddBasicAuth(httpReq, decBasicAuth)
	if addAuthErr != nil {
		log.Printf("Basic auth error %s", addAuthErr)
	}

	res, err = http.DefaultClient.Do(httpReq)

	if err != nil {
		log.Printf("error %s to system/functions %s", method, err)
		return "", err
	}

	defer res.Body.Close()

	log.Printf("Deploy status [%s] - %d", method, res.StatusCode)

	buildStatus, _ := ioutil.ReadAll(res.Body)

	if res.StatusCode < 200 || res.StatusCode > 299 {
		return "", fmt.Errorf("http status code %d, error: %s", res.StatusCode, string(buildStatus))
	}

	return string(buildStatus), err
}
