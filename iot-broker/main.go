package main

import (
	"context"
	"log"

	"github.com/rsocket/rsocket-go"
	"github.com/rsocket/rsocket-go/extension"
	"github.com/rsocket/rsocket-go/payload"
	"github.com/rsocket/rsocket-go/rx/mono"
)

func main() {
	err := rsocket.Receive().
		Resume().
		Fragment(1024).
		Acceptor(func(setup payload.SetupPayload, sendingSocket rsocket.CloseableRSocket) (rsocket.RSocket, error) {
			// bind responder
			return rsocket.NewAbstractSocket(
				rsocket.RequestResponse(func(msg payload.Payload) mono.Mono {
					meta, _ := msg.Metadata()
					auth, err := extension.ParseAuthentication(meta)
					if err == nil {
						authPay := auth.Payload()
						log.Println("Meta: ", string(authPay), auth.IsWellKnown())

					} else {
						log.Println("error metadata")
					}
					return mono.Just(msg)
				}),
			), nil
		}).
		Transport("tcp://127.0.0.1:8080").
		Serve(context.Background())
	panic(err)
}
