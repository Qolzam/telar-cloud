import React from 'react';
import { Link } from "react-router-dom";
import { Button } from 'reactstrap';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUserSecret } from "@fortawesome/free-solid-svg-icons";
import { ReplicasProgress } from "../ReplicasProgress";

const genLogPath = ({ name, gitOwner, gitRepo, gitSha}) => (
  `${gitOwner}/${name}/function-log?repoPath=${gitOwner}/${gitRepo}&commitSHA=${gitSha}`
);

const genFnDetailPath = ({ name, owner }) => (
  `/${owner}/edge/${name}`
);

const genRepoUrl = ({ gitRepoURL, gitBranch }) => {
  if(gitBranch === "") {
    return `${gitRepoURL}/commits/master`
  }
  return `${gitRepoURL}/commits/${gitBranch}`
};

const genOwnerInitials = (owner) => (
  (owner.split(/[-_]+/).length > 1) ?
    owner.split(/[-_]+/)[0].substring(0, 1).concat(owner.split(/[-_]+/)[1].substring(0, 1)) :
      owner.split(/[-_]+/)[0].substring(0, 2)

);

const DeviceTableItem = ({ onClick, fn }) => {
  const {
    name,
    namespace,
    status,
    gitPrivate,
    endpoint,
    sinceDuration,
    invocationCount,
  } = fn;

  const repoUrl = genRepoUrl(fn);
  const logPath = genLogPath(fn);
  const fnDetailPath = genFnDetailPath(fn);

  const handleClick = () => onClick(fnDetailPath);

  return (
    <tr onClick={handleClick} className="cursor-pointer">
      <td>
          { fn.owner}
      </td>
      <td>{name}</td>
      <td>{namespace}</td>
      <td className={"d-none d-sm-table-cell"}>
        { status }
      </td>
      <td>
        <Button
          outline
          size="xs"
          to={logPath}
          onClick={e => e.stopPropagation()}
          tag={Link}
        >
          <FontAwesomeIcon icon="trash" />X
        </Button>
      </td>
    </tr>
  )
};

export {
  DeviceTableItem,
  genOwnerInitials,
};
