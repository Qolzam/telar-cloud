import React, { Component } from 'react';
import { DeviceTable } from '../components/DeviceTable';
import { FunctionEmptyState } from "../components/FunctionEmptyState";
import { devicesApi } from '../api/devicesApi';
import {
  Card,
  CardHeader,
  CardBody,
  CardText,
} from 'reactstrap';
import {faExclamationTriangle} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

export class DeviceOverviewPage extends Component {
  constructor(props) {
    super(props);

    const { user } = props.match.params;
    this.state = {
      isLoading: true,
      fns: [],
      authError: false,
      user,
    };
  }

  componentDidMount() {
    this.setState({ isLoading: true });


    devicesApi.fetchDevices(this.state.user)
    .then(res => {
      let functions = [];
      console.log('--------------------------')
      console.log(res)
      console.log('--------------------------')
      res.forEach( (set) => {
        functions.push(set);
       
      });

      this.setState({ isLoading: false, fns: functions });
    })
    .catch((e) => {
      if (e.response.status === 403) {
        this.setState({isLoading: false, fns: [], authError: true})
      } else {
        console.error(e);
      }
    });
  }

  linkBuilder(location) {
      return `/dashboard/${location}`
  }

  renderContentView() {
    const { user, isLoading, fns, authError } = this.state;

    if (!isLoading && authError) {
      return (
          <Card>
            <CardHeader className="color-failure">
              <FontAwesomeIcon icon={faExclamationTriangle} /> Error: You do not have valid permissions for <span id="username">{ user }</span>
            </CardHeader>
          </Card>
      )
    }

    if (!isLoading && fns.length === 0) {
      return (
        <FunctionEmptyState />
      )
    }

    return (
      <CardBody>
        <CardText>
          IoT Cloud - Devices
        </CardText>
        <DeviceTable isLoading={isLoading} fns={fns} user={user} />
      </CardBody>
    )
  }

  render() {
    const { user } = this.state;

    return (
      <Card outline color="success">
        <CardHeader className="bg-success color-success">
              Edge Devices for {user}
        </CardHeader>

        { this.renderContentView() }
      </Card>
    );
  }
}
