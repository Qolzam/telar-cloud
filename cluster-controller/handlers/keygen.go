package handlers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/Qolzam/openfaas-cloud/sdk"
	"github.com/julienschmidt/httprouter"
)

type DecodeBase64Model struct {
	Data string `json:"data"`
}

// GetKeygenHandle a function invocation
func GetKeygenHandle() func(http.ResponseWriter, *http.Request, httprouter.Params) {

	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		var input []byte

		if r.Body != nil {
			defer r.Body.Close()

			body, _ := ioutil.ReadAll(r.Body)

			input = body
		}

		cipherKey, readCipherKeyErr := sdk.ReadSecret("cipher-key")

		var basicAuth EdgeGateway
		json.Unmarshal(input, &basicAuth)

		if readCipherKeyErr != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(fmt.Sprintf("couldn't get cipher-key: %s", readCipherKeyErr.Error())))
		}
		var encryptErr error
		basicAuth.User, encryptErr = encrypt([]byte(cipherKey), basicAuth.User)
		if encryptErr != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(fmt.Sprintf("couldn't encrypte user: %s", encryptErr.Error())))
		}
		basicAuth.Password, encryptErr = encrypt([]byte(cipherKey), basicAuth.Password)
		if encryptErr != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(fmt.Sprintf("couldn't encrypte password: %s", encryptErr.Error())))
		}

		body, marshalErr := json.Marshal(basicAuth)
		if marshalErr != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(fmt.Sprintf("Can no marshal body: %s", marshalErr.Error())))
		}
		w.WriteHeader(http.StatusOK)
		w.Write(body)
	}
}

// DecodeBase64Handle a function invocation
func DecodeBase64Handle() func(http.ResponseWriter, *http.Request, httprouter.Params) {

	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		var input []byte

		if r.Body != nil {
			defer r.Body.Close()

			body, _ := ioutil.ReadAll(r.Body)

			input = body
		}

		var model DecodeBase64Model
		err := json.Unmarshal(input, &model)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(fmt.Sprintf("Couldn't marshal DecodeBase64Model : %s", err.Error())))
		}

		decodedData, decodeErr := decodeBase64(model.Data)
		if decodeErr != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(fmt.Sprintf("Couldn't decode data : %s", decodeErr.Error())))
		}

		w.WriteHeader(http.StatusOK)
		w.Write(decodedData)
	}
}
