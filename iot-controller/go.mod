module gitlab.com/Qolzam/telar-cloud/iot-controller

go 1.14

require (
	github.com/Azure/go-ansiterm v0.0.0-20170929234023-d6e3b3328b78 // indirect
	github.com/Azure/go-autorest/autorest v0.9.6 // indirect
	github.com/ExpediaDotCom/haystack-client-go v0.0.0-20190315171017-e7edbdf53a61 // indirect
	github.com/Masterminds/goutils v1.1.0 // indirect
	github.com/Masterminds/semver v1.4.2 // indirect
	github.com/Masterminds/sprig v2.22.0+incompatible // indirect
	github.com/Microsoft/hcsshim v0.8.7 // indirect
	github.com/NYTimes/gziphandler v1.1.1 // indirect
	github.com/Qolzam/openfaas-cloud v0.0.0-20200318095548-f1c27b4430c8
	github.com/Shopify/sarama v1.23.1 // indirect
	github.com/VividCortex/gohistogram v1.0.0 // indirect
	github.com/abbot/go-http-auth v0.4.0 // indirect
	github.com/abronan/valkeyrie v0.0.0-20200127174252-ef4277a138cd // indirect
	github.com/alexellis/hmac v0.0.0-20180624211220-5c52ab81c0de // indirect
	github.com/c0va23/go-proxyprotocol v0.9.1 // indirect
	github.com/cenkalti/backoff/v4 v4.0.2 // indirect
	github.com/containerd/containerd v1.3.2 // indirect
	github.com/containous/alice v0.0.0-20181107144136-d83ebdd94cbd // indirect
	github.com/containous/go-http-auth v0.4.0 // indirect
	github.com/containous/traefik/v2 v2.2.1
	github.com/coreos/go-systemd v0.0.0-20191104093116-d3cd4ed1dbcf // indirect
	github.com/docker/cli v0.0.0-20200221155518-740919cc7fc0 // indirect
	github.com/docker/distribution v2.7.1+incompatible // indirect
	github.com/docker/docker v1.13.1 // indirect
	github.com/docker/docker-credential-helpers v0.6.3 // indirect
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/docker/go-metrics v0.0.0-20181218153428-b84716841b82 // indirect
	github.com/docker/libcompose v0.0.0-20190805081528-eac9fe1b8b03 // indirect
	github.com/docker/libtrust v0.0.0-20160708172513-aabc10ec26b7 // indirect
	github.com/donovanhide/eventsource v0.0.0-20170630084216-b8f31a59085e // indirect
	github.com/eapache/channels v1.1.0 // indirect
	github.com/elazarl/go-bindata-assetfs v1.0.0 // indirect
	github.com/evanphx/json-patch v4.5.0+incompatible // indirect
	github.com/flynn/go-shlex v0.0.0-20150515145356-3f9db97f8568 // indirect
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/gambol99/go-marathon v0.0.0-20180614232016-99a156b96fb2 // indirect
	github.com/go-acme/lego/v3 v3.8.0 // indirect
	github.com/go-check/check v0.0.0-20200227125254-8fa46927fb4f // indirect
	github.com/go-kit/kit v0.9.0 // indirect
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e // indirect
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/google/go-github/v28 v28.1.1 // indirect
	github.com/googleapis/gnostic v0.4.2 // indirect
	github.com/gorilla/context v1.1.1 // indirect
	github.com/gorilla/mux v1.7.4 // indirect
	github.com/hashicorp/consul/api v1.3.0 // indirect
	github.com/hashicorp/go-version v1.2.0 // indirect
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/huandu/xstrings v1.2.0 // indirect
	github.com/imdario/mergo v0.3.7 // indirect
	github.com/influxdata/influxdb1-client v0.0.0-20190809212627-fc22c7df067e // indirect
	github.com/instana/go-sensor v1.5.1 // indirect
	github.com/json-iterator/go v1.1.10 // indirect
	github.com/julienschmidt/httprouter v1.3.0
	github.com/kr/pretty v0.2.0 // indirect
	github.com/libkermit/compose v0.0.0-20171122111507-c04e39c026ad // indirect
	github.com/libkermit/docker v0.0.0-20171122101128-e6674d32b807 // indirect
	github.com/libkermit/docker-check v0.0.0-20171122104347-1113af38e591 // indirect
	github.com/magiconair/properties v1.8.1 // indirect
	github.com/mitchellh/copystructure v1.0.0 // indirect
	github.com/mitchellh/hashstructure v1.0.0 // indirect
	github.com/morikuni/aec v0.0.0-20170113033406-39771216ff4c // indirect
	github.com/onsi/gomega v1.8.1 // indirect
	github.com/opencontainers/go-digest v1.0.0-rc1 // indirect
	github.com/opencontainers/image-spec v1.0.1 // indirect
	github.com/opencontainers/runc v1.0.0-rc10 // indirect
	github.com/openfaas/faas v0.0.0-20191125105239-365f459b3f3a // indirect
	github.com/openfaas/faas-provider v0.15.1
	github.com/openzipkin-contrib/zipkin-go-opentracing v0.4.5 // indirect
	github.com/openzipkin/zipkin-go v0.2.2 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible // indirect
	github.com/philhofer/fwd v1.0.0 // indirect
	github.com/rancher/go-rancher-metadata v0.0.0-20200311180630-7f4c936a06ac // indirect
	github.com/sirupsen/logrus v1.6.0 // indirect
	github.com/stvp/go-udp-testing v0.0.0-20191102171040-06b61409b154 // indirect
	github.com/tinylib/msgp v1.0.2 // indirect
	github.com/uber/jaeger-client-go v2.22.1+incompatible // indirect
	github.com/uber/jaeger-lib v2.2.0+incompatible // indirect
	github.com/unrolled/render v1.0.2 // indirect
	github.com/unrolled/secure v1.0.7 // indirect
	github.com/vdemeester/shakers v0.1.0 // indirect
	github.com/vulcand/oxy v1.1.0 // indirect
	go.elastic.co/apm/module/apmot v1.7.0 // indirect
	go.uber.org/goleak v1.0.0 // indirect
	golang.org/x/net v0.0.0-20200625001655-4c5254603344 // indirect
	golang.org/x/sys v0.0.0-20200625212154-ddb9806d33ae // indirect
	golang.org/x/text v0.3.3 // indirect
	golang.org/x/time v0.0.0-20200630173020-3af7569d3a1e // indirect
	google.golang.org/appengine v1.6.6 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
	gopkg.in/DataDog/dd-trace-go.v1 v1.19.0 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/jcmturner/goidentity.v3 v3.0.0 // indirect
	gopkg.in/square/go-jose.v2 v2.5.1 // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
	k8s.io/api v0.18.5
	k8s.io/apimachinery v0.18.5
	k8s.io/client-go v1.5.1
	k8s.io/gengo v0.0.0-20200413195148-3a45101e95ac // indirect
	k8s.io/klog/v2 v2.3.0 // indirect
	k8s.io/utils v0.0.0-20200619165400-6e3d28b6ed19 // indirect
	mvdan.cc/xurls/v2 v2.1.0 // indirect
)

// Contagious forks
replace (
	github.com/abbot/go-http-auth => github.com/containous/go-http-auth v0.4.1-0.20200324110947-a37a7636d23e
	github.com/docker/docker => github.com/moby/moby v0.7.3-0.20190826074503-38ab9da00309
	github.com/go-check/check => github.com/containous/check v0.0.0-20170915194414-ca0bf163426a
	github.com/googleapis/gnostic => github.com/googleapis/gnostic v0.4.0
	github.com/gorilla/mux => github.com/containous/mux v0.0.0-20181024131434-c33f32e26898
	github.com/mailgun/minheap => github.com/containous/minheap v0.0.0-20190809180810-6e71eb837595
	github.com/mailgun/multibuf => github.com/containous/multibuf v0.0.0-20190809014333-8b6c9a7e6bba
	k8s.io/client-go => k8s.io/client-go v0.17.2
)
