package handlers

//noinspection GoUnresolvedReference
import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	traefik "github.com/containous/traefik/v2/pkg/provider/kubernetes/crd/generated/clientset/versioned/typed/traefik/v1alpha1"
	v1alpha1 "github.com/containous/traefik/v2/pkg/provider/kubernetes/crd/traefik/v1alpha1"
	"github.com/julienschmidt/httprouter"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	v1beta1 "k8s.io/api/extensions/v1beta1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	intstr "k8s.io/apimachinery/pkg/util/intstr"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

type CreateEdgeModel struct {
	Name         string `json:"name"`
	EntryPoint   string `json:"entryPoint"`
	Namespace    string `json:"namespace"`
	Owner        string `json:"owner"`
	Token        string `json:"token"`
	Image        string `json:"image"`
	HostSNI      string `json:"hostSNI"`
	PathPrefix   string `json:"pathPrefix"`
	Port         int32  `json:"port"`
	HostWildcard string `json:"hostWildcard"`
}

type CreateEdgeResponseModel struct {
	Crt       string `json:"crt"`
	CrtBase64 string `json:"crtBase64"`
}

type DeleteEdgeModel struct {
	Name      string `json:"name"`
	Namespace string `json:"namespace"`
}

type EdgeModel struct {
	Name      string `json:"name"`
	Namespace string `json:"namespace"`
	Owner     string `json:"owner"`
	Status    string `json:status`
}

type EdgeListModel struct {
	List []EdgeModel `json:"list"`
}

// CreateEdgeHandle a function invocation for creating a device connector
func CreateEdgeHandle() func(http.ResponseWriter, *http.Request, httprouter.Params) {

	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		var input []byte

		if r.Body != nil {
			defer r.Body.Close()

			body, _ := ioutil.ReadAll(r.Body)

			input = body
		}

		var model CreateEdgeModel
		json.Unmarshal(input, &model)

		config, err := rest.InClusterConfig()

		if err != nil {
			fmt.Printf("couldn't get cluster config: %s\n", err)
			os.Exit(-1)
		}

		if err != nil {
			fmt.Printf("couldn't get traefik client: %s\n", err)
			os.Exit(-1)
		}

		clientset, err := kubernetes.NewForConfig(config)
		if err != nil {
			fmt.Printf("couldn't get clientset: %s\n", err)
			os.Exit(-1)
		}

		// Deploy broker
		deployErr := createIoTBrokerDeployment(model.Name, model.Owner, model.Namespace, model.Token, model.Image, clientset)
		if deployErr != nil {
			errorMessage := fmt.Sprintf("\n[ERROR]: Broker server deployment %s", deployErr.Error())
			fmt.Println(errorMessage)
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMessage))
		}

		// Deploy broker service
		serviceErr := createClusterIP(model.Name, model.Namespace, clientset)
		if serviceErr != nil {
			errorMessage := fmt.Sprintf("\n[ERROR]: Broker service %s", serviceErr.Error())
			fmt.Println(errorMessage)
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMessage))
		}

		tlsData, TLSErr := generateTLS(model.HostWildcard)
		if TLSErr != nil {
			errorMessage := fmt.Sprintf("\n[ERROR]: Generate TLS  %s", TLSErr.Error())
			fmt.Println(errorMessage)
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMessage))
		}

		// Deploy broker secret
		secretErr := createSecret(model.Name, model.Namespace, clientset, map[string][]byte{
			"tls.crt": tlsData.Cert,
			"tls.key": tlsData.Key,
		})
		if secretErr != nil {
			errorMessage := fmt.Sprintf("\n[ERROR]: Broker TLS sercret %s", secretErr.Error())
			fmt.Println(errorMessage)
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMessage))
		}

		// Deploy ingress TCP
		ingressTCPErr := createIngressRoute(model.Name,
			model.Namespace,
			model.EntryPoint,
			model.HostSNI,
			model.PathPrefix,
			model.Name,
			model.Name,
			model.Port,
			config)
		if ingressTCPErr != nil {
			errorMessage := fmt.Sprintf("\n[ERROR]: Broker ingress route %s", ingressTCPErr.Error())
			fmt.Println(errorMessage)
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMessage))
		}

		body, marshalErr := json.Marshal(&CreateEdgeResponseModel{
			Crt:       string(tlsData.Cert),
			CrtBase64: encodeBase64(tlsData.Cert),
		})
		if marshalErr != nil {
			errorMessage := fmt.Sprintf("\n[ERROR]: Marshal CreateEdgeResponseModel %s", marshalErr.Error())
			fmt.Println(errorMessage)
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMessage))
		}
		w.WriteHeader(http.StatusOK)
		w.Write(body)
	}
}

// DeleteEdgeHandle a function invocation for creating a device connector
func DeleteEdgeHandle() func(http.ResponseWriter, *http.Request, httprouter.Params) {

	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		var input []byte

		if r.Body != nil {
			defer r.Body.Close()

			body, _ := ioutil.ReadAll(r.Body)

			input = body
		}

		var model DeleteEdgeModel
		json.Unmarshal(input, &model)

		config, err := rest.InClusterConfig()

		if err != nil {
			fmt.Printf("couldn't get cluster config: %s\n", err)
			os.Exit(-1)
		}

		if err != nil {
			fmt.Printf("couldn't get traefik client: %s\n", err)
			os.Exit(-1)
		}

		clientset, err := kubernetes.NewForConfig(config)
		if err != nil {
			fmt.Printf("couldn't get clientset: %s\n", err)
			os.Exit(-1)
		}

		// Delete broker deployment
		deployErr := deleteDeployment(model.Name, model.Namespace, clientset)
		if deployErr != nil {
			errorMessage := fmt.Sprintf("\n[ERROR]: Broker server delete deployment %s", deployErr.Error())
			fmt.Println(errorMessage)
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMessage))
		}

		// Delete broker service
		serviceErr := deleteClusterIP(model.Name, model.Namespace, clientset)
		if serviceErr != nil {
			errorMessage := fmt.Sprintf("\n[ERROR]: Broker delete service %s", serviceErr.Error())
			fmt.Println(errorMessage)
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMessage))
		}

		// Delete broker secret
		secretErr := deleteSecret(model.Name, model.Namespace, clientset)
		if secretErr != nil {
			errorMessage := fmt.Sprintf("\n[ERROR]: Broker delete TLS sercret %s", secretErr.Error())
			fmt.Println(errorMessage)
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMessage))
		}

		// Delete ingress TCP
		ingressTCPErr := deleteIngressTCP(model.Name, model.Namespace, config)
		if ingressTCPErr != nil {
			errorMessage := fmt.Sprintf("\n[ERROR]: Broker delete ingress TCP %s", ingressTCPErr.Error())
			fmt.Println(errorMessage)
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMessage))
		}

		w.WriteHeader(http.StatusOK)
		w.Write([]byte(""))
	}
}

// GetFileHandle a function invocation
func GetEdgeGateway() func(http.ResponseWriter, *http.Request, httprouter.Params) {

	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {

		w.WriteHeader(http.StatusOK)
		w.Write([]byte(""))
	}
}

func createClusterIP(name, ns string, clientset *kubernetes.Clientset) error {
	service := clientset.CoreV1().Services(ns)
	serviceSpec := &corev1.Service{
		TypeMeta: metav1.TypeMeta{
			Kind:       "Service",
			APIVersion: "v1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: ns,
		},
		Spec: corev1.ServiceSpec{
			Type: corev1.ServiceTypeClusterIP,
			Selector: map[string]string{
				"app": "edge_broker",
			},
			Ports: []corev1.ServicePort{
				{
					Name:     "broker",
					Protocol: corev1.ProtocolTCP,
					Port:     8000,
					TargetPort: intstr.IntOrString{
						Type:   intstr.Int,
						IntVal: 8000,
					},
				},
			},
		},
	}
	_, err := service.Create(serviceSpec)

	if err != nil {
		wrappedErr := fmt.Errorf("failed create Service: %s", err.Error())
		log.Println(wrappedErr)
		return wrappedErr

	}

	log.Printf("Service created: %s.%s\n", name, ns)
	return nil
}

func deleteClusterIP(name, namespace string, clientset *kubernetes.Clientset) error {

	service := clientset.CoreV1().Services(namespace)
	fmt.Println("[INFO] Deleting service clusterIP...")
	deletePolicy := metav1.DeletePropagationForeground
	err := service.Delete(name, &metav1.DeleteOptions{
		PropagationPolicy: &deletePolicy,
	})
	if err != nil {
		return err
	}

	fmt.Printf("\n[INFO] Deleted service clusterIP %s in %s", name, namespace)
	return nil
}

// ListEdgeHandle a function invocation for query on device connectors
func ListEdgeHandle() func(http.ResponseWriter, *http.Request, httprouter.Params) {

	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {

		var input []byte

		if r.Body != nil {
			defer r.Body.Close()

			body, _ := ioutil.ReadAll(r.Body)

			input = body
		}

		var model EdgeModel
		json.Unmarshal(input, &model)

		config, err := rest.InClusterConfig()

		if err != nil {
			fmt.Printf("couldn't get cluster config: %s\n", err)
			os.Exit(-1)
		}

		clientset, err := kubernetes.NewForConfig(config)
		if err != nil {
			fmt.Printf("couldn't get clientset: %s\n", err)
			os.Exit(-1)
		}

		listOpts := metav1.ListOptions{
			LabelSelector: "component=edge_broker",
		}

		podList, err := clientset.CoreV1().Pods(model.Namespace).List(listOpts)
		if err != nil {
			fmt.Printf("couldn't list pods in %s: %s\n", model.Namespace, err.Error())
			os.Exit(-1)
		}

		var deviceList EdgeListModel
		for _, item := range podList.Items {
			newDevice := EdgeModel{
				Name:      item.Labels["device_name"],
				Namespace: item.Namespace,
				Status:    podStatus(item.Status.Phase),
				Owner:     item.Labels["owner"],
			}

			deviceList.List = append(deviceList.List, newDevice)
			fmt.Printf("\nThe function is: %s - status: %v - %v", item.Labels["device_name"], item.Status.ContainerStatuses, item.Status.Phase)
		}

	}
}

func createIngressTCP(name, ns, entryPoint, hostSNI, serviceName, secret string, port int32, config *rest.Config) error {
	traefikClient, err := traefik.NewForConfig(config)
	if err != nil {
		return err
	}
	ingressRouteTCPClient := traefikClient.IngressRouteTCPs(ns)

	ingress := &v1alpha1.IngressRouteTCP{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
		Spec: v1alpha1.IngressRouteTCPSpec{
			EntryPoints: []string{entryPoint},
			Routes: []v1alpha1.RouteTCP{
				{
					Match: fmt.Sprintf("HostSNI(`%s`)", hostSNI),
					Services: []v1alpha1.ServiceTCP{
						{
							Name: serviceName,
							Port: port,
						},
					},
				},
			},
			TLS: &v1alpha1.TLSTCP{
				SecretName: secret,
			},
		},
	}
	_, errIngressTCPCreate := ingressRouteTCPClient.Create(ingress)
	if errIngressTCPCreate != nil {
		return errIngressTCPCreate
	}

	return nil
}

func deleteIngressTCP(name, namespace string, config *rest.Config) error {
	traefikClient, err := traefik.NewForConfig(config)
	if err != nil {
		return err
	}
	ingressRouteTCPClient := traefikClient.IngressRouteTCPs(namespace)
	fmt.Println("[INFO] Deleting ingress TCP...")
	deletePolicy := metav1.DeletePropagationForeground
	deleteErr := ingressRouteTCPClient.Delete(name, &metav1.DeleteOptions{
		PropagationPolicy: &deletePolicy,
	})
	if deleteErr != nil {
		return deleteErr
	}

	fmt.Printf("\n[INFO] Deleted ingress TCP %s in %s", name, namespace)
	return nil
}

func createIngressRoute(name, ns, entryPoint, hostSNI, pathPrefix, serviceName, secret string, port int32, config *rest.Config) error {
	traefikClient, err := traefik.NewForConfig(config)
	if err != nil {
		return err
	}
	ingressRouteClient := traefikClient.IngressRoutes(ns)

	ingress := &v1alpha1.IngressRoute{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
		Spec: v1alpha1.IngressRouteSpec{
			EntryPoints: []string{entryPoint},
			Routes: []v1alpha1.Route{
				{
					Match: fmt.Sprintf("HostSNI(`%s`) && PathPrefix(`%s`)", hostSNI, pathPrefix),
					Kind:  "Rule",
					Services: []v1alpha1.Service{
						{
							Name: serviceName,
							Port: port,
						},
					},
				},
			},
			TLS: &v1alpha1.TLS{
				SecretName: secret,
			},
		},
	}
	_, errIngressTCPCreate := ingressRouteClient.Create(ingress)
	if errIngressTCPCreate != nil {
		return errIngressTCPCreate
	}

	return nil
}

func deleteIngressRoute(name, namespace string, config *rest.Config) error {
	traefikClient, err := traefik.NewForConfig(config)
	if err != nil {
		return err
	}
	ingressRouteTCPClient := traefikClient.IngressRouteTCPs(namespace)
	fmt.Println("[INFO] Deleting ingress TCP...")
	deletePolicy := metav1.DeletePropagationForeground
	deleteErr := ingressRouteTCPClient.Delete(name, &metav1.DeleteOptions{
		PropagationPolicy: &deletePolicy,
	})
	if deleteErr != nil {
		return deleteErr
	}

	fmt.Printf("\n[INFO] Deleted ingress TCP %s in %s", name, namespace)
	return nil
}

func createIngress(name, ns, path string, clientset *kubernetes.Clientset) error {
	ingressClient := clientset.ExtensionsV1beta1().Ingresses(ns)
	ingress := &v1beta1.Ingress{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
		Spec: v1beta1.IngressSpec{
			Rules: []v1beta1.IngressRule{
				{
					IngressRuleValue: v1beta1.IngressRuleValue{
						HTTP: &v1beta1.HTTPIngressRuleValue{
							Paths: []v1beta1.HTTPIngressPath{
								{
									Path: path,
									Backend: v1beta1.IngressBackend{
										ServiceName: "service1",
										ServicePort: intstr.FromInt(80),
									},
								},
							},
						},
					},
				},
			},
		},
	}
	_, err := ingressClient.Create(ingress)
	if err != nil {
		return err
	}

	return nil
}

func createIoTBrokerDeployment(edgeDeviceName, owner, namespace, token, image string, clientset *kubernetes.Clientset) error {

	deploymentsClient := clientset.AppsV1().Deployments(namespace)
	labels := make(map[string]string)
	labels["app"] = edgeDeviceName
	labels["app"] = "edge_broker"
	labels["owner"] = owner
	labels["app.kubernetes.io/name"] = edgeDeviceName
	labels["app.kubernetes.io/app"] = "edge_broker"
	labels["app.kubernetes.io/part-of"] = namespace

	metaDeploy := metav1.ObjectMeta{
		Name:      edgeDeviceName,
		Namespace: namespace,
		Labels:    labels,
	}
	deployment := &appsv1.Deployment{
		ObjectMeta: metaDeploy,
		Spec: appsv1.DeploymentSpec{
			Replicas: int32Ptr(1),
			Selector: &metav1.LabelSelector{
				MatchLabels: map[string]string{
					"edge_name": edgeDeviceName,
					"app":       "edge_broker",
				},
			},
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Namespace: namespace,
					Annotations: map[string]string{
						"prometheus.io.scrape": "false",
					},
					Labels: map[string]string{
						"edge_name": edgeDeviceName,
						"app":       "edge_broker",
					},
				},
				Spec: corev1.PodSpec{
					Volumes: []corev1.Volume{
						{
							Name: "auth",
							VolumeSource: corev1.VolumeSource{
								Secret: &corev1.SecretVolumeSource{
									SecretName: "basic-auth",
								},
							},
						},
					},
					Containers: []corev1.Container{
						{
							Name:            edgeDeviceName,
							Image:           image,
							ImagePullPolicy: "IfNotPresent",
							Resources: corev1.ResourceRequirements{
								Limits: corev1.ResourceList{
									"cpu":    resource.MustParse("100m"),
									"memory": resource.MustParse("100Mi"),
								},
								Requests: corev1.ResourceList{
									"cpu":    resource.MustParse("100m"),
									"memory": resource.MustParse("100Mi"),
								},
							},
							Env: []corev1.EnvVar{
								{
									Name:  "TOKEN",
									Value: token,
								},
							},
							Ports: []corev1.ContainerPort{
								{ContainerPort: 8080},
							},
						},
					},
				},
			},
		},
	}

	// Create Deployment
	fmt.Println("Creating deployment...")

	result, err := deploymentsClient.Create(deployment)
	if err != nil {
		return err
	}
	fmt.Printf("Created deployment %q.\n", result.GetObjectMeta().GetName())

	return nil
}

func deleteDeployment(name, namespace string, clientset *kubernetes.Clientset) error {

	deploymentsClient := clientset.AppsV1().Deployments(namespace)
	fmt.Println("[INFO] Deleting deployment...")
	deletePolicy := metav1.DeletePropagationForeground
	err := deploymentsClient.Delete(name, &metav1.DeleteOptions{
		PropagationPolicy: &deletePolicy,
	})
	if err != nil {
		return err
	}

	fmt.Printf("\n[INFO] Deleted deployment %s in %s", name, namespace)
	return nil
}

func createSecret(name, ns string, clientset *kubernetes.Clientset, data map[string][]byte) error {
	secret := &corev1.Secret{
		TypeMeta: metav1.TypeMeta{
			Kind:       "Secret",
			APIVersion: "v1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: ns,
		},
		Data: data,
		Type: "Opaque",
	}
	secretOut, err := clientset.CoreV1().Secrets(ns).Create(secret)
	if secretOut != nil {
		fmt.Println("[ERROR]: Create secret ", err)
	}
	return nil
}

func deleteSecret(name, namespace string, clientset *kubernetes.Clientset) error {

	secretClient := clientset.CoreV1().Secrets(namespace)
	fmt.Println("[INFO] Deleting secret...")
	deletePolicy := metav1.DeletePropagationForeground
	err := secretClient.Delete(name, &metav1.DeleteOptions{
		PropagationPolicy: &deletePolicy,
	})
	if err != nil {
		return err
	}

	fmt.Printf("\n[INFO] Deleted secret %s in %s", name, namespace)
	return nil
}
