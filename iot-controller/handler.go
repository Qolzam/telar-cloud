package function

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/Qolzam/telar-cloud/iot-controller/handlers"
)

var router *httprouter.Router

// Handler function
func Handle(w http.ResponseWriter, r *http.Request) {

	// Server Routing
	if router == nil {

		router = httprouter.New()

		// Devices
		router.POST("/devices", handlers.CreateDeviceHandle())
		router.GET("/devices", handlers.GetDviceStatusListHandle())

		// Keygen
		router.POST("/keygen", handlers.GetKeygenHandle())
		router.GET("/decode", handlers.DecodeBase64Handle())

		// Edge Gateway
		router.POST("/edge", handlers.CreateEdgeHandle())
		router.DELETE("/edge", handlers.DeleteEdgeHandle())
		router.GET("/edge", handlers.GetDviceStatusListHandle())
	}
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "'X-Requested-With, X-HTTP-Method-Override, Accept, Content-Type,access-control-allow-origin, access-control-allow-headers")

	router.ServeHTTP(w, r)
}
