module handler

go 1.14

replace handler/function => ./function

require github.com/gorilla/websocket v1.4.2
